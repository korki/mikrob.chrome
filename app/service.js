var Mikrob = (Mikrob || {});
Mikrob.Service = (function(){
	var blipAcc;
	var last_id; 
	var embedly = new Embedly();
	var load_attempt = 0;
	var username;
	var dashboardOffset, userOffset, tagOffset, currentTag, currentUser;
	var followed;

	var OAuthReq = new OAuthRequest({
		consumerKey : BlipOAuthData.key,
		consumerSecret : BlipOAuthData.secret,
		urlConf : BlipOAuthData.url
	});

	function loadDashboard(callbackAfter) {
		this.blipAcc.getDashboard(false, {
			onSuccess : function(resp) {
				if(resp.length > 0) {
					resp.forEach(function(stat){
						App.statusStore.store(stat.id, stat);
					});
					Mikrob.Controller.renderDashboard(resp, false);
					last_id = resp[0].id;
					Mikrob.Controller.detectGlobalTimeOffset(resp[0].created_at);
					callbackAfter();
				}
				load_attempt = 0;
			},
			onFailure : function(resp) {
				if(load_attempt < 2) Mikrob.Notification.create("Mikrob", 'Nie mogę załadować kokpitu!');
				load_attempt += 1;
				console.dir(resp);
			}
		});
	}

	function updateDashboard() {
		this.blipAcc.getDashboard(last_id, {
			onSuccess : function(resp) {
				if(resp.length > 0) {
					// cache
					resp.forEach(function(stat) {
						App.statusStore.store(stat.id, stat);
					});

					Mikrob.Controller.renderDashboard(resp, true);
					last_id = resp[0].id;
					load_attempt = 0;
					Mikrob.Controller.updateRelativeTime();
				}
			},
			onFailure : function(resp) {
				if(load_attempt < 6) {
					Mikrob.Notification.create("Mikrob", 'Wystąpił błąd podczas pobierania kokpitu');
				} else {
					Mikrob.Controller.offlineMode();
					Settings.check.canPoll = false;
					load_attempt = 0;
				}
				load_attempt += 1;
				console.dir(resp);
			}
		});
	}
	
	function loadPage(callbackAfter) {
		if (undefined == dashboardOffset) {
			dashboardOffset = Settings.check.messagesPerPage;
		}
		if (dashboardOffset > 400) {
			Mikrob.Notification.create("Mikrob", 'Nie można pobrać więcej wiadomości.');
			callbackAfter();
			return;
		}
		
		this.blipAcc.getDashboard(false, {
			onSuccess : function(resp) {
				if(resp.length > 0) {
					// cache
					resp.forEach(function(stat) {
						App.statusStore.store(stat.id, stat);
					});
					
					Mikrob.Controller.renderDashboard(resp, false);
					dashboardOffset += Settings.check.messagesPerPage;
					callbackAfter();
				}
			},
			onFailure : function(resp) {
				Mikrob.Notification.create("Mikrob", 'Wystąpił błąd podczas pobierania kokpitu');
				console.dir(resp);
			}
		},
		dashboardOffset);
	}
	
	function loadUserPage(username, callbackAfter) {
		if (undefined == userOffset) {
			userOffset = Settings.check.messagesPerPage;
		}
		if (undefined == currentUser) {
			currentUser = username;
		} else if (currentUser != username) {
			currentUser = username;
			userOffset = Settings.check.messagesPerPage;
		}
		if (userOffset > 400) {
			Mikrob.Notification.create("Mikrob", 'Nie można pobrać więcej wiadomości.');
			callbackAfter();
			return;
		}
		
		this.blipAcc.statusesOf(username, {
			onSuccess : function(resp) {
				if(resp.length > 0) {
					Mikrob.Controller.showUserStatuses(resp);
					userOffset += Settings.check.messagesPerPage;
					callbackAfter();
				}
			},
			onFailure : function(resp) {
				Mikrob.Notification.create("Mikrob", 'Wystąpił błąd podczas pobierania wiadomości');
				console.dir(resp);
			}
		}, null,
		userOffset);
	}

	function loadTagPage(tag, callbackAfter) {
		if (undefined == tagOffset) {
			tagOffset = Settings.check.messagesPerPage;
		}
		if (undefined == currentTag) {
			currentTag = username;
		} else if (currentTag != tag) {
			currentTag = tag;
			tagOffset = Settings.check.messagesPerPage;
		}
		if (tagOffset > 400) {
			Mikrob.Notification.create("Mikrob", 'Nie można pobrać więcej wiadomości.');
			callbackAfter();
			return;
		}
		
		this.blipAcc.tag(NormalizePolishChars(tag), false, {
			onSuccess : function(resp) {
				if(resp.length > 0) {
					console.log(resp);
					Mikrob.Controller.showTag(resp);
					tagOffset += Settings.check.messagesPerPage;
					callbackAfter();
				}
			},
			onFailure : function(resp) {
				Mikrob.Notification.create("Mikrob", 'Wystąpił błąd podczas pobierania wiadomości');
				console.dir(resp);
			}
		}, null,
		tagOffset);
	}

	function createStatus(body, file, callbacks) {
		this.blipAcc.createStatus(body, file, callbacks);
	}
	
	function getSingleStatus(id, callbacks) {
		var single = App.statusStore.get(id);
		if(single) {
			callbacks.onSuccess(single);
		} else {
			this.blipAcc.getStatus(id, callbacks);
		}
	}

	function getCurrentUsername(blip, after) {
		this.blipAcc = blip;
		this.blipAcc.getCurrentUsername({
			onSuccess : function(resp) {
				this.username = resp[0].user.login;
				after();
			}.bind(this),
			onFailure : function(resp) {
				console.dir(resp);
			}
		});
	}

	function getUserInfo(username, callbacks) {
		// FIXME no caching yet!
		this.blipAcc.userInfo(username, callbacks);
	}

	function getGeoLocation() {
		navigator.geolocation.getCurrentPosition(function(geo) {
			// blip style geo
			var str = ["@/", geo.coords.latitude, ',', geo.coords.longitude, '/'].join('');
			Mikrob.Controller.setContents(str, false, true);

		});
	}
	
	function getFollowed(callback) {
		if (followed != undefined && Object.keys(followed).length > 0) {
			callback(followed);
		} else {
			followed = new Object();
			this.blipAcc.followed({
				onSuccess : function(resp) {
					resp.forEach(function(user) {
						followed[user.tracked_user_path.replace('/users/', '')] = user.tracked_user_path.replace('/users/', '');
					});
					callback(followed);
				},
				onFailure : function() {
					console.log("getFollowed failure");
				}
			});
		}
	}
	
	function followUser(username) {
		this.blipAcc.follow(username, {
			onSuccess : function() {
				Mikrob.Notification.create('Mikrob', ['Dodano', username, 'do obserwowanych'].join(' '));
			},
			onFailure : function() {
				Mikrob.Notification.create('Mikrob', 'Błąd dodawania do obserwowanych');
			}
		});
	}

	function unfollowUser(username) {
		this.blipAcc.unfollow(username, {
			onSuccess : function() {
				Mikrob.Notification.create('Mikrob', ['Usunięto', username, 'z obserwowanych'].join(' '));
			},
			onFailure : function() {
				Mikrob.Notification.create('Mikrob', 'Błąd usuwania z obserwowanych');
			}
		});
	}

	function ignoreUser(username) {
		this.blipAcc.ignore(username, {
			onSuccess : function() {
				Mikrob.Notification.create('Mikrob', ['Dodano', username, 'do ignorowanych'].join(' '));
			},
			onFailure : function() {
				Mikrob.Notification.create('Mikrob', 'Błąd dodawania do ignorowanych');
			}
		});
	}

	function unignoreUser(username) {
		this.blipAcc.unignore(username, {
			onSuccess : function() {
				Mikrob.Notification.create('Mikrob', ['Usunięto', username, 'z ignorowanych'].join(' '));
			},
			onFailure : function() {
				Mikrob.Notification.create('Mikrob', 'Błąd usuwania z ignorowanych');
			}
		});
	}
	function processThread(obj) {
		function userObject(name) {
			if(name) {
				return {
					login : name,
					avatar : {
						url_50 : ('/users/'+name+'/avatar/pico.jpg')
					}
				};
			} else {
				return false;
			}
		}
		function convertToStatus(_id, object) {
			var username = object.user.split("/").reverse()[0];
			var recipient = false;
			if (object.recipient) {
				recipient = object.recipient.split("/").reverse()[0];
			}
			return {
				body : object.content,
				created_at : object.create_date,
				id : _id,
				user : userObject(username),
				recipient : userObject(recipient)
			};
		}

		var discussion = obj.discussion;
		discussion[obj.blipid] = {
			content : obj.content,
			create_date : obj.create_date,
			user : obj.user,
			recipient : obj.recipient
		};

		var list = [], res = [];
		for(var id in discussion) {
			list.push(id);
		}
		list.sort().forEach(function(id){
			var o = convertToStatus(id, discussion[id]);
			res.push(o);
		});

		return res;
	}

	function getTag(tag) {

		this.blipAcc.tag(NormalizePolishChars(tag), false,{
			onSuccess : function(resp) {
				if(resp.length > 0) {
					Mikrob.Controller.renderTag(tag, resp);
				}
			},
			onFailure : function(resp) {
				console.dir(resp);
				Mikrob.Notification.create('Mikrob', 'Problem z pobraniem statusów z tagu: #' + tag);
			}
		});
	}
	
	function getSubscribedTags(callback) {
		var subscribed = new Object();
		this.blipAcc.tagSubscribed({
			onSuccess : function(resp) {
				resp.forEach(function(tag) {
					subscribed[tag.tag_path.replace('/tags/', '')] = tag.tag_path.replace('/tags/', '');
				});
				callback(subscribed);
			},
			onFailure : function() {
				console.log("getSubscribedTags failure");
			}
		});
	}	

	function tagAction(action, tag) {
		var fail = function() {
			Mikrob.Notification.create('Mikrob', 'Problem z działaniem na tag\'u #' + tag);
		};
		var callbacksSub = {
			onSuccess : function() {
				Mikrob.Notification.create('Mikrob', 'Zasubskrybowano tag #' + tag);
			},
			onFailure : fail
		};

		var callbacksNone = {
			onSuccess : function() {
				Mikrob.Notification.create('Mikrob', 'Usunięto subskrybcję tagu #' + tag);
			},
			onFailure : fail
		};
		switch(action) {
			case 'all':
				this.blipAcc.tagSubscribeAll(tag, callbacksSub);
				break;
			case 'tracked':
				this.blipAcc.tagSubscribeTracked(tag, callbacksSub);
				break;
			case 'none':
				this.blipAcc.tagIgnore(tag, callbacksNone);
				break;

			default:
				break;
		}
	}

	function deleteStatus(id) {
		this.blipAcc.remove(id,{
			onSuccess : function() {
				Mikrob.Controller.removeStatus(id);
			},
			onFailure : function() {
				Mikrob.Notification.create('Mikrob', 'Nie udało się usunąć statusu!');
			}
		});
	}
	
	function likeStatus(id) {
		this.blipAcc.like(id, {
			onSuccess : function() {
				Mikrob.Notification.create('Mikrob', 'Status został polubiony');
				/*
				poczebna jest metoda co pobierze jeszcze raz status i odswiezy widok pojedynczego wpisu tak zeby:
				1. mial odpowiednia liczbe polubien
				2. zmienilo sie serduszko na pelne
				3. zmienila sie metoda do wywalania na unlike
				*/
			},
			onFailure : function() {
				Mikrob.Notification.create('Mikrob', 'Nie udało się polubić statusu!');
			}
		});
	}	

	function shortlinkExpand(id,element, callback) {

		var url = App.shortlinkStore.get(id);
		if( url) {
			callback(element, url);
		} else {
			this.blipAcc.expandShortlink(id,{
				onSuccess : function(linkData) {
					App.shortlinkStore.store(id, linkData.original_link);
					callback(element, linkData.original_link);
				},
				onFailure : function(){
					// remove expand action from
					// unexpandable link
					callback(element, false);
				}
			});
		}
	}

	function shortlinkCreate(url, callback) {
		this.blipAcc.createShortlink(url, {
			onSuccess : function(data) {
				var id = data.url.split('/').reverse()[0];
				App.shortlinkStore.store('id', url);
				callback(url, data.url);
			},
			onFailure : function() {}
		});
	}

	function showMedia(url) {
		embedly.getCode(url, {
			onSuccess : function(object) {
				console.dir(object);
				object.original_link = url;
				Mikrob.Controller.showMedia('embed', object);
			},
			onFailure : function() {
				Platform.openURL(url);
			}
		});
	}

	return {
		OAuthReq : OAuthReq,
		blipAcc : blipAcc,
		embedly : embedly,
		username : username,
		getCurrentUsername : getCurrentUsername,
		loadDashboard : loadDashboard,
		updateDashboard : updateDashboard,
		createStatus : createStatus,
		deleteStatus : deleteStatus,
		likeStatus : likeStatus,
		getSingleStatus : getSingleStatus,
		getUserInfo : getUserInfo,
		getGeoLocation : getGeoLocation,
		getFollowed: getFollowed,
		followUser : followUser,
		unfollowUser : unfollowUser,
		ignoreUser : ignoreUser,
		unignoreUser : unignoreUser,
		getTag : getTag,
		getSubscribedTags: getSubscribedTags,
		tagAction : tagAction,
		shortlinkExpand : shortlinkExpand,
		shortlinkCreate : shortlinkCreate,
		showMedia : showMedia,
		loadPage: loadPage,
		loadUserPage: loadUserPage,
		loadTagPage: loadTagPage
	};
})();
